# DTAP : Debug & Test Access Port

This spin-off of the YGREC8 project provides an asynchronous logic block to interface a digital design under test (DUT) with a host through a half-duplex SPI-like interface (3 or 4 wires).

This is not compatible with JTAG and uses a different organisation. It is also a bit simpler. But mostly : it does not require your design to use "Built-In Logic Block Observer" (BILBO) gates !

For more, see the logs/docs at https://hackaday.io/project/193122-dtap-debug-and-test-access-port or the manual (stay tuned)